#include <stdio.h>

#include "mem.h"
#include "mem_internals.h"

int counter = 0;

// Test 1: Successful memory allocation.
char* test_successful_allocation() {
    printf("Test 1: Memory allocated successfully.\n");
    void* ptr = _malloc(100);
    if (ptr != NULL) {
        _free(ptr);
        counter++;
        return "Test 1 passed";
    } else {
        return "Test 1 failed: Memory allocation failed";
    }
    heap_term();
}

// Test 2: Freeing a single block out of multiple allocated blocks.
char* test_free_single_block() {
    printf("Test 2: Freeing a single block out of multiple allocated blocks.\n");
    void* ptr1 = _malloc(10);
    void* ptr2 = _malloc(10);
    _free(ptr2);
    struct block_header* first_block = (struct block_header*)(ptr1 - offsetof(struct block_header, contents));
    struct block_header* second_block = (struct block_header*)(ptr2 - offsetof(struct block_header, contents));

    if (!second_block->is_free || first_block->next != second_block) {
        return "Test 2 failed";
    } else {
        counter++;
        return "Test 2 passed";
    }
    heap_term();
}

// Test 3: Freeing two blocks out of multiple allocated blocks.
char* test_free_multiple_blocks() {
    printf("Test 3: Freeing two blocks out of multiple allocated blocks.\n");
    void* ptr1 = _malloc(10);
    void* ptr2 = _malloc(10);
    _free(ptr1);
    _free(ptr2);
    struct block_header* first_block = (struct block_header*)(ptr1 - offsetof(struct block_header, contents));
    struct block_header* second_block = (struct block_header*)(ptr2 - offsetof(struct block_header, contents));

    if (!second_block->is_free || !first_block->is_free || first_block->next != second_block) {
        return "Test 3 failed";
    } else {
        counter++;
        return "Test 3 passed";
    }
    heap_term();
}

// Test 4: Memory exhausted, new memory region extends the old one.
char* test_extend_existing_region() {
    printf("Test 4: Memory exhausted, new memory region extends the old one.\n");
    heap_init(100);
    void* ptr = _malloc(200);
    if (HEAP_START + offsetof(struct block_header, contents) != ptr) {
        return "Test 4 failed";
    } else {
        counter++;
        return "Test 4 passed";
    }
    heap_term();
}

// Test 5: Memory exhausted, old memory region cannot extend due to another allocated address range.
char* test_new_region_in_different_location() {
    printf("Test 5: Memory exhausted, old memory region cannot extend due to another allocated address range.\n");
    void* ptr = heap_init(0);
    struct block_header* block = ptr;
    void* data = mmap(ptr + REGION_MIN_SIZE, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);
    _malloc(REGION_MIN_SIZE + 2);
    if (!data || !block->is_free) {
        return "Test 5 failed";
    } else {
        counter++;
        return "Test 5 passed";
    }
    heap_term();
}

int main() {
    printf("Start tests:\n");

    char* result;

    result = test_successful_allocation();
    printf("%s\n", result);

    result = test_free_single_block();
    printf("%s\n", result);

    result = test_free_multiple_blocks();
    printf("%s\n", result);

    result = test_extend_existing_region();
    printf("%s\n", result);

    result = test_new_region_in_different_location();
    printf("%s\n", result);

    printf("Count of passed tests: %d/5\n", counter);

    return 0;
}